
## Browse, explore and experiment in the realm of JPA, Hibernate and Spring Data JPA ##

### What is this repository for? ###

* Obviously there are tons of high - quality tutorials on this subject, but I was too tempted to set the playground from scratch
* The idea behind this repo is to have a skeletal structure for a JPA based application, enhanced with Spring Data Jpa 
* Separate branches might serve as a good starting point for various JPA experiments

### How do I get set up? ###

* Plain old maven project, requires Java 1.7