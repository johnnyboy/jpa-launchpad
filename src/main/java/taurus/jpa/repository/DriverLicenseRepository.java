package taurus.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import taurus.jpa.domain.dirty.checking.DriverLicense;

@Repository
public interface DriverLicenseRepository extends
		CrudRepository<DriverLicense, Long> {

}
