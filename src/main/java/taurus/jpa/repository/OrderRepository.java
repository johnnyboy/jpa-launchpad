package taurus.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import taurus.jpa.domain.table.vs.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
