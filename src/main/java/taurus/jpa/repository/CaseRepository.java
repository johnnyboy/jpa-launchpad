package taurus.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import taurus.jpa.domain.table.vs.entity.Case;

public interface CaseRepository extends CrudRepository<Case, Long> {

}
