package taurus.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import taurus.jpa.domain.dirty.checking.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

}
