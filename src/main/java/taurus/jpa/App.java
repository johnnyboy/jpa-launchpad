package taurus.jpa;

import org.springframework.context.support.GenericXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		context.getEnvironment().setActiveProfiles("dev");
		context.load("jpa-context.xml");
		context.refresh();

		context.close();
	}

}
