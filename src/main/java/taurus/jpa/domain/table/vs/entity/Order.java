package taurus.jpa.domain.table.vs.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "Orders")
public class Order {

	@Id
	@GeneratedValue
	private long id;

	@Basic(optional = false)
	private String description;

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", description=" + description + "]";
	}

}
