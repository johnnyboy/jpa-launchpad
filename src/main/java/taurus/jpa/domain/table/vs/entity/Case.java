package taurus.jpa.domain.table.vs.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Caze")
public class Case {

	@Id
	@GeneratedValue
	private long id;

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Case [id=" + id + "]";
	}
}
