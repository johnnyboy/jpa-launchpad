package taurus.jpa.domain.dirty.checking;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name = "DriverLicense")
public class DriverLicense {

	@Id
	@GeneratedValue
	private long id;

	@OneToOne
	@Basic(optional = false)
	private Person holder;

	@Basic(optional = false)
	private DateTime issuedOn;

	public long getId() {
		return id;
	}

	public Person getHolder() {
		return holder;
	}

	public void setHolder(Person holder) {
		this.holder = holder;
	}

	public DateTime getIssuedOn() {
		return issuedOn;
	}

	public void setIssuedOn(DateTime issuedOn) {
		this.issuedOn = issuedOn;
	}

	@Override
	public String toString() {
		return String.format("DriverLicense{id:%d, holder:%d, issuedOn:%t}",
				id, holder.getId(), issuedOn);
	}
}
