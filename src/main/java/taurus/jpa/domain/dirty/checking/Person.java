package taurus.jpa.domain.dirty.checking;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Person")
public class Person {

	@Id
	@GeneratedValue
	private long id;

	@Basic(optional = false)
	private String name;

	@Basic(optional = false)
	private Integer age;

	@Enumerated(value = EnumType.STRING)
	private Gender gender;

	@OneToOne(cascade = CascadeType.PERSIST)
	private DriverLicense driverLicense;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public DriverLicense getDriverLicense() {
		return driverLicense;
	}

	public void setDriverLicense(DriverLicense driverLicense) {
		this.driverLicense = driverLicense;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return String.format("Person{id:%d, %sname:%s, age:%d, gender:%s}", id,
				driverLicenseId(), name, age, gender);
	}

	private String driverLicenseId() {
		return String.format(
				"%s",
				(driverLicense == null) ? "" : String.format(
						"driverLicense:%s, ", driverLicense.getId()));
	}

}
