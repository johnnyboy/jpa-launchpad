package taurus.jpa.domain.dirty.checking;

public enum Gender {
	MALE, FEMALE
}
