package taurus.jpa.domain.one.to.one;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class CustomerFixed {

	@Id
	@GeneratedValue
	private long id;

	@OneToOne
	@JoinColumn(name = "CUSTOMERS_ADDRESS")
	private AddressFixed addressFixed;
}