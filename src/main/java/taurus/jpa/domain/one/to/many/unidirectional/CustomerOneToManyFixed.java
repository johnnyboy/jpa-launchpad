package taurus.jpa.domain.one.to.many.unidirectional;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class CustomerOneToManyFixed {

	@Id
	@GeneratedValue
	private long id;

	@OneToMany
	@JoinColumn(name = "CUSTOMER")
	private Set<AddressOneToManyFixed> addresses;
}
