package taurus.jpa.domain.one.to.one;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Address {

	@Id
	@GeneratedValue
	private long id;

	@OneToOne
	private Customer customer;
}
