package taurus.jpa.domain.one.to.many.bidirectional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AddressManyToOneBidirectional {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	private CustomerOneToManyBidirectional customer;
}
