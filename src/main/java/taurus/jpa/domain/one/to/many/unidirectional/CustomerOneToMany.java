package taurus.jpa.domain.one.to.many.unidirectional;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CustomerOneToMany {

	@Id
	@GeneratedValue
	private long id;

	@OneToMany
	private Set<AddressOneToMany> addresses;
}
