package taurus.jpa.domain.one.to.many.bidirectional;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CustomerOneToManyBidirectional {
	
	@Id
	@GeneratedValue
	private long id;
	
	@OneToMany
	private Set<AddressManyToOneBidirectional> addresses;
}
