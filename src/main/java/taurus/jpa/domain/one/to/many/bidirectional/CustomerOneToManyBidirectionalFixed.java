package taurus.jpa.domain.one.to.many.bidirectional;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CustomerOneToManyBidirectionalFixed {

	@Id
	@GeneratedValue
	private long id;

	@OneToMany(mappedBy = "customer")
	private Set<AddressManyToOneBidirectionalFixed> addresses;
}
