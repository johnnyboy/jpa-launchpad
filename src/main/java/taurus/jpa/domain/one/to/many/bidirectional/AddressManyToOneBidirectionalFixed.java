package taurus.jpa.domain.one.to.many.bidirectional;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AddressManyToOneBidirectionalFixed {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	@JoinColumn(name = "CUSTOMER", foreignKey = @ForeignKey(name = "FK_ADDRESS_CUSTOMER"))
	private CustomerOneToManyBidirectionalFixed customer;
}
