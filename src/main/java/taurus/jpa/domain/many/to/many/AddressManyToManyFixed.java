package taurus.jpa.domain.many.to.many;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class AddressManyToManyFixed {

	@Id
	@GeneratedValue
	private long id;

	@ManyToMany(mappedBy = "addresses")
	private Set<CustomerManyToManyFixed> customers;
}
