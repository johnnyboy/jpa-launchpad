package taurus.jpa.domain.many.to.many;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class CustomerManyToManyFixed {

	@Id
	@GeneratedValue
	private long id;

	@ManyToMany
	@JoinTable(
			name = "CUSTOMERS_ADDRESSES", 
			joinColumns = @JoinColumn(name = "CUSTOMER_ADDRESS", referencedColumnName = "id"), 
			inverseJoinColumns = @JoinColumn(name = "ADDRESS_CUSTOMER", referencedColumnName = "id"))
	private Set<AddressManyToManyFixed> addresses;
}
