package taurus.jpa.domain.many.to.many;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class CustomerManyToMany {

	@Id
	@GeneratedValue
	private long id;

	@ManyToMany
	private Set<AddressManyToMany> addresses;
}
