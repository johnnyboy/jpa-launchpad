package taurus.jpa.service;


public interface PersonService {
	
	void transactionalUpdate(long id);
	
	void transactionlessUpdate(long id);
}
