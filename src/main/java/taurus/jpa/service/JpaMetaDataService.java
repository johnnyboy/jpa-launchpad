package taurus.jpa.service;

public interface JpaMetaDataService {

	boolean tableExists(String tableName);

	boolean entityExists(String entityName);

	boolean foreignKeyExists(Class<?> primaryEntityClass,
			Class<?> foreignEntityClass);

	String joinTableName(Class<?> primaryEntityClass,
			Class<?> foreignEntityClass);
}
