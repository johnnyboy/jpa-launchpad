package taurus.jpa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import taurus.jpa.domain.dirty.checking.Gender;
import taurus.jpa.domain.dirty.checking.Person;
import taurus.jpa.repository.PersonRepository;
import taurus.jpa.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	@Transactional
	public void transactionalUpdate(long id) {
		Person person = personRepository.findOne(id);
		person.setGender(Gender.FEMALE);
	}

	@Override
	public void transactionlessUpdate(long id) {
		Person person = personRepository.findOne(id);
		person.setGender(Gender.FEMALE);
	}

}
