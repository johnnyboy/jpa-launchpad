package taurus.jpa.service.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;

import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Joiner;

import taurus.jpa.service.JpaMetaDataService;

@Service
public class JpaMetadataServiceImpl implements JpaMetaDataService {

	@PersistenceContext(name = "jpa-launchpad")
	private EntityManager em;

	@Override
	@Transactional
	public boolean tableExists(String tableName) {
		Session session = em.unwrap(Session.class);
		return session.doReturningWork(new DatabaseTableChecker(tableName));
	}

	@Override
	public boolean entityExists(String entityName) {
		Set<EntityType<?>> types = em.getMetamodel().getEntities();
		Collection<String> names = new HashSet<>(types.size());
		Iterator<EntityType<?>> it = types.iterator();
		while (it.hasNext()) {
			names.add(it.next().getName());
		}
		return names.contains(entityName);
	}

	@Override
	@Transactional
	public boolean foreignKeyExists(Class<?> primaryEntityClass,
			Class<?> foreignEntityClass) {
		Session session = em.unwrap(Session.class);
		return session.doReturningWork(new ForeignKeyChecker(primaryEntityClass
				.getSimpleName(), foreignEntityClass.getSimpleName()));
	}

	@Override
	public String joinTableName(Class<?> primaryEntityClass,
			Class<?> foreignEntityClass) {
		return Joiner.on('_').join(primaryEntityClass.getSimpleName(),
				foreignEntityClass.getSimpleName());
	}

	private static class DatabaseTableChecker implements ReturningWork<Boolean> {

		private final String tableName;

		DatabaseTableChecker(String tableName) {
			this.tableName = tableName;
		}

		@Override
		public Boolean execute(Connection conn) throws SQLException {
			DatabaseMetaData metaData = conn.getMetaData();
			ResultSet tables = metaData.getTables(null, null, null, null);
			while (tables.next()) {
				String dbTableName = tables.getString("TABLE_NAME");
				if (tableName.equalsIgnoreCase(dbTableName)) {
					return true;
				}
			}
			return false;
		}
	}

	private static class ForeignKeyChecker implements ReturningWork<Boolean> {

		private final String primaryTableName;

		private final String foreignTableName;

		ForeignKeyChecker(String primaryTableName, String foreignTableName) {
			this.primaryTableName = primaryTableName;
			this.foreignTableName = foreignTableName;
		}

		@Override
		public Boolean execute(Connection conn) throws SQLException {
			DatabaseMetaData metaData = conn.getMetaData();
			ResultSet foreignKeys = metaData.getImportedKeys(null, null,
					primaryTableName.toUpperCase());
			while (foreignKeys.next()) {
				if (foreignTableName.equalsIgnoreCase(foreignKeys
						.getString("PKTABLE_NAME"))) {
					return true;
				}
			}
			return false;
		}

	}

}
