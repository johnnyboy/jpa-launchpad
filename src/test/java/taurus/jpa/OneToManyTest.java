package taurus.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import taurus.jpa.domain.one.to.many.bidirectional.AddressManyToOneBidirectional;
import taurus.jpa.domain.one.to.many.bidirectional.AddressManyToOneBidirectionalFixed;
import taurus.jpa.domain.one.to.many.bidirectional.CustomerOneToManyBidirectional;
import taurus.jpa.domain.one.to.many.bidirectional.CustomerOneToManyBidirectionalFixed;
import taurus.jpa.domain.one.to.many.unidirectional.AddressOneToMany;
import taurus.jpa.domain.one.to.many.unidirectional.AddressOneToManyFixed;
import taurus.jpa.domain.one.to.many.unidirectional.CustomerOneToMany;
import taurus.jpa.domain.one.to.many.unidirectional.CustomerOneToManyFixed;

public class OneToManyTest extends DatabaseIntegrationTest {

	@Test
	public void unidirectionalOneToManyCreatesJoinTable() {
		String joinTableName = jpaMetaDataService.joinTableName(
				CustomerOneToMany.class, AddressOneToMany.class);
		assertThat(jpaMetaDataService.tableExists(joinTableName)).isTrue();
	}

	@Test
	public void bidirectionalOneToManyCreatesJoinTable() {
		String joinTableName = jpaMetaDataService.joinTableName(
				CustomerOneToManyBidirectional.class,
				AddressManyToOneBidirectional.class);
		assertThat(jpaMetaDataService.tableExists(joinTableName)).isTrue();
	}

	@Test
	public void bidirectionalOneToManyWithJoinColumnDoesNotCreateJoinTable() {
		String joinTableName = jpaMetaDataService.joinTableName(
				CustomerOneToManyBidirectionalFixed.class,
				AddressManyToOneBidirectionalFixed.class);
		assertThat(jpaMetaDataService.tableExists(joinTableName)).isFalse();
	}

	@Test
	public void bidirectionalOneToManyJoinColumnCanBeDeclaredOnTheManySide() {
		assertThat(
				jpaMetaDataService.foreignKeyExists(
						AddressManyToOneBidirectionalFixed.class,
						CustomerOneToManyBidirectionalFixed.class)).isTrue();
		assertThat(
				jpaMetaDataService.foreignKeyExists(
						CustomerOneToManyBidirectionalFixed.class,
						AddressManyToOneBidirectionalFixed.class)).isFalse();
	}

	@Test
	public void unidirectionalOneToManyWithJoinColumnDoesNotCreateJoinTable() {
		String joinTableName = jpaMetaDataService.joinTableName(
				CustomerOneToManyFixed.class, AddressOneToManyFixed.class);
		assertThat(jpaMetaDataService.tableExists(joinTableName)).isFalse();
	}

	@Test
	public void unidirectionalOneToManyWithJoinColumnCreatesForeignKeyOnTheManySide() {
		assertThat(
				jpaMetaDataService.foreignKeyExists(
						AddressOneToManyFixed.class,
						CustomerOneToManyFixed.class)).isTrue();
	}
}
