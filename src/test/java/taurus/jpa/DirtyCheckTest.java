package taurus.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import taurus.jpa.domain.dirty.checking.Gender;
import taurus.jpa.domain.dirty.checking.Person;
import taurus.jpa.repository.PersonRepository;
import taurus.jpa.service.PersonService;

public class DirtyCheckTest extends DatabaseIntegrationTest {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private PersonService personService;

	@Test
	public void dirtyCheckingExists() {
		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus tx = transactionManager.getTransaction(txDef);

		Person bob = persistPerson();

		bob.setGender(Gender.FEMALE);

		transactionManager.commit(tx);

		Person dbBob = personRepository.findOne(bob.getId());
		assertThat(dbBob.getGender()).isEqualTo(Gender.FEMALE);
	}

	@Test
	public void dirtyCheckDoesNotWorkForNonManagedEntity() {
		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus tx = transactionManager.getTransaction(txDef);

		Person bob = persistPerson();

		transactionManager.commit(tx);

		assertThat(em.contains(bob)).isFalse();
		bob.setGender(Gender.FEMALE);

		Person dbBob = personRepository.findOne(bob.getId());
		assertThat(dbBob.getGender()).isEqualTo(Gender.MALE);
	}

	@Test
	public void dirtyCheckingExistsOnTransactionalMethod() {
		Person bob = persistPerson();
		personService.transactionalUpdate(bob.getId());
		Person dbBob = personRepository.findOne(bob.getId());
		assertThat(dbBob.getGender()).isEqualTo(Gender.FEMALE);
	}

	@Test
	public void noDirtyCheckingOnNonTransactionalMethod() {
		Person bob = persistPerson();
		personService.transactionlessUpdate(bob.getId());
		Person dbBob = personRepository.findOne(bob.getId());
		assertThat(dbBob.getGender()).isEqualTo(Gender.MALE);
	}

	private Person persistPerson() {
		Person bob = new Person();
		bob.setName("Bob");
		bob.setAge(25);
		bob.setGender(Gender.MALE);
		return personRepository.save(bob);
	}
}
