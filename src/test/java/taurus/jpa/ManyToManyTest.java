package taurus.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import taurus.jpa.domain.many.to.many.AddressManyToMany;
import taurus.jpa.domain.many.to.many.AddressManyToManyFixed;
import taurus.jpa.domain.many.to.many.CustomerManyToMany;
import taurus.jpa.domain.many.to.many.CustomerManyToManyFixed;

public class ManyToManyTest extends DatabaseIntegrationTest {

	@Test
	public void manyToManyWithoutOwningSideAndExplicitJoinTableCreatesTwoJoinTables() {
		String customerAddress = jpaMetaDataService.joinTableName(
				CustomerManyToMany.class, AddressManyToMany.class);
		String addressCustomer = jpaMetaDataService.joinTableName(
				AddressManyToMany.class, CustomerManyToMany.class);
		assertThat(jpaMetaDataService.tableExists(customerAddress)).isTrue();
		assertThat(jpaMetaDataService.tableExists(addressCustomer)).isTrue();
	}

	@Test
	public void manyToManyWithOwningSideAndExplicitJoinTableCreatesOneJoinTable() {
		String customerAddress = jpaMetaDataService.joinTableName(
				CustomerManyToManyFixed.class, AddressManyToManyFixed.class);
		String addressCustomer = jpaMetaDataService.joinTableName(
				AddressManyToManyFixed.class, CustomerManyToManyFixed.class);
		assertThat(jpaMetaDataService.tableExists(customerAddress)).isFalse();
		assertThat(jpaMetaDataService.tableExists(addressCustomer)).isFalse();
		assertThat(jpaMetaDataService.tableExists("CUSTOMERS_ADDRESSES"))
				.isTrue();
	}
}
