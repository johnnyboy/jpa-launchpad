package taurus.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import taurus.jpa.domain.table.vs.entity.Case;
import taurus.jpa.domain.table.vs.entity.Order;
import taurus.jpa.repository.CaseRepository;
import taurus.jpa.repository.OrderRepository;

public class TableVsEntityTest extends DatabaseIntegrationTest {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private CaseRepository caseRepository;

	@Test
	public void entityNameAffectsBothDatabaseAndJPA() {
		// given
		Order o = new Order();
		o.setDescription("Dummy description");
		orderRepository.save(o);

		// when
		TypedQuery<Order> query = em.createQuery("SELECT o FROM Orders o",
				Order.class);
		List<Order> orders = query.getResultList();

		// then
		assertThat(orders).isNotNull().isNotEmpty();
		assertThat(jpaMetaDataService.tableExists("Orders")).isTrue();
		assertThat(jpaMetaDataService.entityExists("Orders")).isTrue();
		assertThat(jpaMetaDataService.entityExists("Order")).isFalse();
	}

	@Test
	public void tableNameAffectsOnlyDatabase() {
		// given
		caseRepository.save(new Case());

		// when
		TypedQuery<Case> query = em.createQuery("SELECT c FROM Case c",
				Case.class);
		List<Case> cases = query.getResultList();

		// then
		assertThat(cases).isNotNull().isNotEmpty();
		assertThat(jpaMetaDataService.tableExists("Caze")).isTrue();
		assertThat(jpaMetaDataService.entityExists("Case")).isTrue();
		assertThat(jpaMetaDataService.entityExists("Caze")).isFalse();
	}

}
