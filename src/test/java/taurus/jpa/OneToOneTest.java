package taurus.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import taurus.jpa.domain.one.to.one.Address;
import taurus.jpa.domain.one.to.one.AddressFixed;
import taurus.jpa.domain.one.to.one.Customer;
import taurus.jpa.domain.one.to.one.CustomerFixed;

public class OneToOneTest extends DatabaseIntegrationTest {

	@Test
	public void biDirectionalOneToOneCreatesForeignKeysInBothTables() {
		assertThat(
				jpaMetaDataService.foreignKeyExists(Customer.class,
						Address.class)).isTrue();
		assertThat(
				jpaMetaDataService.foreignKeyExists(Address.class,
						Customer.class)).isTrue();
	}

	@Test
	public void biDirectionalOneToOneWithMappedByCreatesForeignKeyOnlyOnTheOwningSide() {
		assertThat(
				jpaMetaDataService.foreignKeyExists(CustomerFixed.class,
						AddressFixed.class)).isTrue();
		assertThat(
				jpaMetaDataService.foreignKeyExists(AddressFixed.class,
						CustomerFixed.class)).isFalse();
	}
}
