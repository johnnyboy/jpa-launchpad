package taurus.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;

import taurus.jpa.service.JpaMetaDataService;

@ContextConfiguration(locations = "classpath:jpa-context.xml")
@ActiveProfiles(value = "test")
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class DatabaseIntegrationTest {

	@PersistenceContext(name = "jpa-launchpad")
	protected EntityManager em;

	@Autowired
	protected PlatformTransactionManager transactionManager;

	@Autowired
	protected JpaMetaDataService jpaMetaDataService;

}